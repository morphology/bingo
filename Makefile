all : ${HOME}/bin/bingo.pyz

%.pyz :
	shiv -o $@ -e bingo.main:main .

@lint :
	tox -e lint
