import pytest

from pathlib import Path

import fitz

from bingo.control_file import ControlFile
from bingo.playback import create_playback
from bingo.typeset import create_pdf, BingoCardSet
from modernize_images.ffprobe import ffprobe

SONGS_DIR = Path(__file__).parent / "../../songs"


def test_write_control_file(tmp_path):
    """
    Should write and read a CSV based on a list of paths.
    """
    control_file_path = tmp_path / "control.csv"
    control_file = ControlFile.from_paths(
        ["2019/19_001 Lizzo - Juice.mp3", "2019/19_002 Billie Eilish - bad guy.mp3"]
    )
    control_file.write(control_file_path)
    assert control_file_path.exists()
    control_file2 = ControlFile(control_file_path)
    assert len(control_file.records) == len(control_file2.records)


@pytest.mark.parametrize("duration", [10, 20, 30])
def test_combine_clips(duration, tmp_path, request):
    """
    Should produce a single file that's not very long.
    """
    ogg_path = tmp_path / f"{request.node.name}.ogg"
    input_paths = [
        {"path": SONGS_DIR / p, "duration": duration}
        for p in [
            "1990/90_01 Wilson Phillips - Hold On.mp3",
            "1990/90_02 Roxette - It Must Have Been Love.mp3",
            "1990/90_03 Sinead O'Connor - Nothing Compares 2 U.mp3",
        ]
    ]
    create_playback(input_paths, ogg_path)
    n_segments = len(input_paths)
    duration_s = ffprobe(ogg_path).get_duration().total_seconds()
    assert (n_segments - 1) * duration <= duration_s <= (n_segments + 1) * duration


@pytest.mark.parametrize("n_cards", [None, 100])
def test_bingo_card_set_pdf(n_cards, tmp_path, request):
    """
    Should produce valid TeX which can become a PDF.
    """
    control_file_path = Path(__file__).parent / f"{request.node.name}.csv"
    control_file = ControlFile(control_file_path)
    assert len(control_file.records) >= 24

    bingo_card_set = BingoCardSet(control_file)
    output_pdf = tmp_path / f"{request.node.name}.pdf"
    create_pdf(bingo_card_set, n_cards=n_cards, output_pdf=output_pdf)

    with fitz.open(output_pdf) as fi:
        if n_cards is None:
            assert len(control_file.records) / 24 < fi.page_count
        else:
            assert n_cards == fi.page_count
