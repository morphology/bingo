"""
Generate musical bingo from a list of songs
===========================================

[blue]
1. Create a control file from your songs. Every run will be the same.[/]

    find /your/music/directory -type f | bingo create-control-file -o control.csv

[blue]
2. Edit that CSV in a spreadsheet program:[/]

    - The category "artist" is for one-hit wonders. Default bingo text is the artist name.
    - The category "duet" is for duets. Default bingo text is the song name.
    - The category "song" is for songs by popular artists. Default bingo text is the song name.

    Edit those any way you like! Delete or duplicate rows, change bingo categories, etc.

[blue]
3. Create a playback file from the control file. By default, more-popular songs play for 8 seconds, and less-popular
   songs play for 20 or 30 seconds. Every run will be different.[/]

    bingo create-playback control.csv first-playback-directory
    bingo create-playback control.csv second-playback-directory

[blue]
4. Create a PDF with bingo cards from the control file. By default, all cards are generated. You can generate
   a random subset with -n. Every run will be different.[/]

    bingo create-pdf control.csv bingo.pdf
    bingo create-pdf control.csv bingo.pdf -n 100

"""
import argparse
import fileinput
import logging
import random
import sys
from pathlib import Path

from rich.progress import Progress
from rich.logging import RichHandler
from rich_argparse_plus import RichHelpFormatterPlus

from .control_file import ControlFile
from .playback import create_playback
from .typeset import BingoCardSet, create_pdf
from ._version import __version__

logger = logging.getLogger(__name__)


def _get_parser():
    interactive = sys.stdin.isatty()
    RichHelpFormatterPlus.choose_theme("the_matrix")
    parser = argparse.ArgumentParser(
        prog="bingo",
        formatter_class=RichHelpFormatterPlus,
        description=__doc__,
    )
    parser.add_argument("-v", "--verbose", action="count", default=0)
    parser.add_argument(
        "--version", action="version", version=f"%(prog)s {__version__}"
    )
    subparsers = parser.add_subparsers(
        dest="command", description="Each step has its own command:"
    )
    subparsers.required = True

    control_parser = subparsers.add_parser(
        "create-control-file", help="Step 1: create CSV for you to edit"
    )
    control_parser.add_argument("-o", "--csv-out", type=Path, metavar="CSV")
    control_parser.add_argument("lists", nargs="+" if interactive else "*", type=Path)

    playback_parser = subparsers.add_parser(
        "create-playback", help="Step 3: create music files"
    )
    playback_parser.add_argument("control_file", type=ControlFile, metavar="CSV")
    playback_parser.add_argument("out_dir", type=Path, metavar="NEW-FOLDER")

    typeset_parser = subparsers.add_parser(
        "create-pdf", help="Step 4: create a PDF of bingo cards"
    )
    typeset_parser.add_argument("control_file", type=ControlFile, metavar="CSV")
    typeset_parser.add_argument("out_pdf", type=Path, metavar="PDF")
    typeset_parser.add_argument(
        "-n",
        "--n-cards",
        type=int,
        help="Randomly print (n) cards. If not specified, all cards are generated",
    )

    return parser


def main():
    args = _get_parser().parse_args()
    log_level = logging.WARNING - 10 * args.verbose
    logging.basicConfig(
        format="%(levelname)s: %(message)s",
        handlers=[RichHandler()],
        level=log_level,
    )

    match args.command:
        case "create-control-file":
            paths = []
            for line in fileinput.input(files=args.lists):
                line = line.rstrip()
                if line:
                    paths.append(line)
            control_file = ControlFile.from_paths(paths)
            control_file.records.sort(key=lambda r: (r["sort_artist"], r["song"]))
            control_file.write(args.csv_out)
        case "create-playback":
            control_file = args.control_file
            total_duration = sum(r["duration"] for r in control_file.records)
            n_entries = len(control_file.records)
            average_duration = int(total_duration / n_entries)
            segments_per_file = 20 * 60 // average_duration  # minutes

            with (progress := Progress()):
                random.shuffle(control_file.records)
                args.out_dir.mkdir(parents=True, exist_ok=True)

                for n in progress.track(
                    range(0, n_entries, segments_per_file),
                    description="Creating song clips...",
                ):
                    path_out = args.out_dir / f"bingo-{n:03d}.ogg"
                    create_playback(
                        control_file.records[n : n + segments_per_file],
                        path_out,
                    )
        case "create-pdf":
            bingo_card_set = BingoCardSet(args.control_file)
            create_pdf(bingo_card_set, n_cards=args.n_cards, output_pdf=args.out_pdf)
        case _:
            return "Not implemented"
