import logging
import os
import subprocess
from typing import List, Optional


def create_playback(
    records: List[dict],
    output_path: os.PathLike,
    answers_path: Optional[os.PathLike] = None,
    fade: float = 3,
):
    answers_path = answers_path or output_path.with_suffix(".txt")
    n_segments = len(records)
    input_args = ["-nostdin"]
    filter_parts = [f"[0][1]acrossfade=d={fade}:c1=tri:c2=tri[a01]"]
    for n, record in enumerate(records):
        duration = record.get("duration") or 10.0
        input_args += ["-t", str(duration), "-i", record["path"]]
        if 1 <= n < n_segments - 2:
            filter_parts.append(
                f"[a{n:02d}][{n+1}]acrossfade=d={fade}:c1=tri:c2=tri[a{n+1:02d}]"
            )
    filter_parts.append(
        f"[a{n_segments-2:02d}][{n_segments-1}]acrossfade=d={fade}:c1=tri:c2=tri"
    )
    command = [
        "ffmpeg",
        *input_args,
        "-vn",
        "-filter_complex",
        ";".join(filter_parts),
        "-y",
        output_path,
    ]
    proc = subprocess.run(command, capture_output=True)
    if proc.returncode != 0:
        logging.error(f"{command} exited {proc.returncode}")
        if proc.stderr:
            logging.error(proc.stderr.decode())
    else:
        logging.debug(f"{command} exited {proc.returncode}")
        if proc.stderr:
            logging.debug(proc.stderr.decode())
    with open(answers_path, "w") as fo:
        for n, record in enumerate(records, start=1):
            fo.write(f"{n:2d}\t{record['artist']}\t{record['song']}\n")
    return proc.returncode == 0
