"""
Create bingo cards in ReportLab
"""
import os
import random

from reportlab.lib import colors
from reportlab.lib.enums import TA_CENTER
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet, ParagraphStyle
from reportlab.platypus import (
    PageBreak,
    SimpleDocTemplate,
    Table,
    TableStyle,
    Paragraph,
)
from reportlab.lib.units import inch
from rich.progress import Progress

from .control_file import ControlFile


class BingoCard:
    def __init__(self, columns):
        self.columns = columns

    def get_rows(self):
        n_columns = n_rows = len(self.columns)
        for c in range(n_columns):
            assert len(self.columns[c]) == n_rows
        for r in range(n_rows):
            yield tuple(self.columns[c][r] for c in range(n_columns))


class BingoCardSet:
    def __init__(self, control_file: ControlFile):
        self.control_file = control_file

    def generate_all_cards(self):
        entries = [r["bingo"] for r in self.control_file.records]
        random.shuffle(entries)

        while entries and len(entries) >= 24:
            column1 = entries[:5]
            column2 = entries[5:10]
            column3 = entries[10:12] + ["free"] + entries[12:14]
            column4 = entries[14:19]
            column5 = entries[19:24]
            yield BingoCard([column1, column2, column3, column4, column5])
            entries.pop(0)

    def generate_random_cards(self, n_cards: int):
        return random.choices(list(self.generate_all_cards()), k=n_cards)


def create_pdf(bingo_card_set: BingoCardSet, n_cards: int, output_pdf: os.PathLike):
    if n_cards is None:
        cards = bingo_card_set.generate_all_cards()
    else:
        cards = bingo_card_set.generate_random_cards(n_cards)

    doc = SimpleDocTemplate(str(output_pdf), pagesize=letter)

    styles = getSampleStyleSheet()

    styles.add(
        ParagraphStyle(
            name="Normal_CENTER",
            parent=styles["Normal"],
            alignment=TA_CENTER,
        )
    )
    story = []
    tblstyle = TableStyle(
        [
            ("ALIGN", (0, 0), (-1, -1), "CENTER"),
            ("VALIGN", (0, 0), (-1, -1), "MIDDLE"),
            ("INNERGRID", (0, 1), (-1, -1), 0.25, colors.black),
            ("BOX", (0, 1), (-1, -1), 0.25, colors.black),
        ]
    )

    width = (8.5 - 1) / 6 * inch
    headings = list("BINGO")

    with (progress := Progress()):
        for card in progress.track(cards, description="Generating cards..."):
            data = [headings]
            for row in card.get_rows():
                data.append([Paragraph(t, style=styles["Normal_CENTER"]) for t in row])
            tbl = Table(data, colWidths=[width] * 5, rowHeights=[width] * 6)
            tbl.setStyle(tblstyle)
            story.append(tbl)

            story.append(PageBreak())
        progress.console.status("Writing PDF...")
        doc.build(story)
