import csv
import string
from collections import Counter
import os
from pathlib import Path
import re
from typing import Optional


class ControlFile:
    def __init__(self, path=None):
        self.fields = None
        self.records = []
        if path:
            self.read(path)
            assert self.fields

    def read(self, path):
        path = Path(path)
        self.records = []
        with open(path) as control_file:
            reader = csv.reader(control_file, dialect="excel")
            self.fields = next(reader)
            for r in reader:
                record = dict(zip(self.fields, r))
                record["duration"] = float(record["duration"])
                record["path"] = Path(record["path"])
                self.records.append(record)

    def write(self, path):
        path = Path(path)
        with open(path, "w", newline="") as control_file:
            writer = csv.DictWriter(control_file, fieldnames=self.fields)
            writer.writeheader()
            for record in self.records:
                artists = record["artists"]
                writer.writerow({**record, "artists": " & ".join(artists)})

    def guess_categories(self):
        if "category" not in self.fields:
            self.fields.append("category")
        if "bingo" not in self.fields:
            self.fields.append("bingo")

        category_count = Counter()
        song_count_by_artist = Counter(r["sort_artist"] for r in self.records)
        for record in self.records:
            if not (category := record.get("category")):
                if len(record["artists"]) > 1:
                    category = "duet"
                    bingo = "{} -- {}".format(record["artist"], record["song"])
                elif song_count_by_artist[record["sort_artist"]] > 1:
                    # Popular artist
                    category, bingo = "artist", record["song"]
                else:
                    category = "one-hit-wonder"
                    bingo = "{} -- {}".format(record["artist"], record["song"])

                record["category"], record["bingo"] = category, bingo
            category_count[category] += 1
        print("Song categories:")
        for name, count in category_count.most_common():
            print(f"  {count:3d} {name}")

    @classmethod
    def from_paths(cls, paths):
        new_control_file = cls()
        new_control_file.fields = [
            "order",
            "path",
            "duration",  # seconds
            "artist",
            "artists",  # array of different artist names
            "sort_artist",  # flattened for sorting, with "A" and "The" removed
            "song",
            "rank",
            "year",
        ]

        title_and_artist = Counter()
        n_skipped = 0
        for order, path in enumerate(paths, start=1):
            if not (record := get_metadata(path)):
                print(f"Failed to gather info for {path}")
                n_skipped += 1
                continue
            # Deduplicate
            index = (record["sort_artist"], record["song"].capitalize())
            if title_and_artist[index] > 0:
                print(f"Skipping {path}")
                n_skipped += 1
                continue

            # Duration of the beginning when cross-fading playback
            rank = record["rank"]
            if rank <= 10:
                duration = 8.0
            elif 10 < rank < 39:
                duration = 20.0
            else:
                duration = 30.0

            new_control_file.records.append(
                {
                    "order": order,
                    "path": path,
                    "duration": duration,
                    **record,
                    "category": None,
                    "bingo": None,
                }
            )
            title_and_artist[index] += 1
        print(f"Skipped {n_skipped} duplicate song(s)")
        new_control_file.guess_categories()
        return new_control_file


def get_metadata(path: os.PathLike) -> Optional[dict]:
    alnum = string.ascii_letters + string.digits
    charted_pattern = re.compile(
        r"(?P<year>\d{4})[/\\]\d{2}_[AB]?(?P<rank>\d{1,3}) (?P<artist>.*) - (?P<song>.*)"
    )
    duet_pattern = re.compile(r" and | & ", re.IGNORECASE)
    feat_pattern = re.compile(r"featuring | feat[.]? | ft[.] ", re.IGNORECASE)

    path = Path(path)
    m = charted_pattern.match(str(path.with_suffix("")))
    if not (m or path.is_file()):
        return
    record = m.groupdict()

    artist, *feat = feat_pattern.split(record["artist"])
    sort_artist = re.sub(r"^(A|The)\s+", "", artist.strip(), flags=re.IGNORECASE)
    while sort_artist and sort_artist[0] not in alnum:
        sort_artist = sort_artist[1:]
    return {
        **record,
        "rank": int(record["rank"]),
        "sort_artist": sort_artist,
        "artists": tuple(duet_pattern.split(artist)),
    }
